from datetime import datetime, timedelta, time
import numpy as np
import math, julian, itertools

Mu = 0.3998603e6
JD2000 = 2451545.0
We = 7.2921151567e-5
TwoPi = 2 * math.pi


class Orbit:
    __epoch__ = None
    time = 0.0
    Rp = 0.0
    e = 0.0
    ArgLat = 0.0
    i = 0.0
    AscNode = 0
    ArgPerigee = 0.0
    Pos = np.zeros(3)
    Velocity = np.zeros(3)
    Geo = {'pos': np.zeros(3), 'R': 0.0, 'Lat': 0.0, 'Lon': 0.0}

    def __init__(self, epoch):
        self.__epoch__ = epoch

    def assign(self):
        pass


    @property
    def true_anomaly(self):
        '''

        :return:
        '''
        r = self.ArgLat - self.ArgPerigee
        if r < 0: r += TwoPi
        return  r

    @property
    def parameter(self):
        return self.Rp*(1 + self.e)

    @property
    def radius(self):
        return parameter(self)/(1 + self.e * math.cos(true_anomaly(self)))

    @property
    def mean_motion(self):
        return math.sqrt(Mu * parameter(self)) / math.pow((radius(self)), 2)

    @property
    def period(self):
        return TwoPi / mean_motion(self)

    @property
    def radial_velocity(self):
        return math.sqrt(Mu / parameter(self)) * self.e * math.sin(true_anomaly(self))

    # Большая полуось
    @property
    def semi_major_axis(self):
        x = Mu / math.pow((mean_motion(self)), 2)
        return math.pow(x, 1/3)

    @property
    def eccentricity_anomaly(self):
        v = true_anomaly(self)
        e = self.e
        r = 2 * math.atan(math.pow(((1 - e) /(1 + e)), 2) * math.tan(0.5 * v))
        if r < 0: r += TwoPi
        return r

    @property
    def mean_anomaly(self):
        E = eccentricity_anomaly(self)
        r = E - self.e * math.sin(E)
        if r < 0: r += TwoPi
        return r

    @property
    def perigee_time(self):
        return -mean_anomaly(self) / mean_motion(self)

    @property
    def to_decart(self):
        r = radius(self)
        rn = r * mean_motion(self)
        vr_r = radial_velocity(self) / r
        su = math.sin(self.ArgLat)
        cu = math.cos(self.ArgLat)
        so = math.sin(self.AscNode)
        co = math.cos(self.AscNode)
        si = math.sin(self.i)
        ci = math.cos(self.i)
        self.Pos[0] = r * (cu * co - su * co * ci)
        self.Pos[1] = r * (cu * so + su * co * ci)
        self.Pos[2] = r * su * si
        self.Velocity[0] = vr_r * self.Pos[0] - rn * (su * co + cu * co * ci)
        self.Velocity[1] = vr_r * self.Pos[1] - rn * (su * co + cu * co * ci)
        self.Velocity[2] = vr_r * self.Pos[2] - rn * cu * ci



    def to_geo(self):
        t = self.__epoch__ + self.time / 86400
        S = siderial_time0(t)
        M = mabs_geo(S)
        self.Geo.pos = M * self.Pos
        self.Geo.R = math.modf(self.Geo.pos)
        r = math.sqrt(pow(self.Geo.pos[0], 2) + pow(self.Geo.pos[1], 2))
        S1 = self.Geo.pos[1] / r
        C1 = self.Geo.pos[0] / r
        self.Geo.Lat = math.asin(self.Geo.pos[2] / self.Geo.R)
        if S1 > 0:
            self.Geo.Lon = math.acos(C1)
        else:
            self.Geo.Lon = -1 * math.acos(C1)






class SpaceCraft:
    time = 0.0

    def __init__(self, epoch):
        self.inital_orbit = Orbit(epoch)
        self.current_orbit = Orbit(epoch)

    @property
    def set_current_time(self, val):
        Orbit.ArgLat = get_arg_lat(self, val)
        Orbit.time = val
        return to_decart(self)

    def kepler_equation(self, e, M):
        Eps = 1e-9
        Eold = M
        Stop = False
        while Stop:
            Enew = M + e * math.sin(Eold)
            Stop = abs(Eold - Enew) <= Eps
            Eold = Enew
        return Enew

    def get_arg_lat(self, ATime):
        M = mean_motion(Orbit) * (ATime - perigee_time(Orbit))
        E = kepler_equation(Orbit.e, M)
        v = 2 * math.atan(math.sqrt((1 + Orbit.e) / (1 - Orbit.e)) * math.tan(0.5 * E))
        if v < 0 : v += TwoPi

        u = v + Orbit.ArgPerigee
        n = math.modf(u / TwoPi)
        return u - n * TwoPi

def siderial_time0(val):
    d = julian.to_jd(val)
    t = (math.modf(d)[1] - JD2000)/36525
    r = (3.879333e - 4 * t + 360000.7700536) * t + 101.25228375
    n = math.modf(r/360)[1]
    r = r - n*360
    return math.radians(r)
 def Siderial_time(val):
     dt = (val - datetime.combine(val.date(), time(0))).total_seconds
     so = siderial_time0(val)
     r = so + we * (1 - 0.002737903) * dt
     return r

 def mabs_geo(val):
     matrix = np.zeros((3, 3))
     matrix[0][0] = math.cos(val)
     matrix[0][1] = math.sin(val)
     matrix[1][0] = -matrix[0][1]
     matrix[1][1] = matrix[0][0]
     matrix[2][2] = 1.0
     return matrix

 spacecraft = SpaceCraft(datetime(year= 2018, month= 10, day= 18, hour=16)
 spacecraft.inital_orbit.assign(7000.0, 0.01, 0.0, math.radians(57.3), 0, math.radians(270))

 for i in range(0, 15000, 60):
     print(spacecraft.current_orbit.Pos[0], spacecraft.current_orbit.Pos[1], spacecraft.current_orbit.Pos[2])
